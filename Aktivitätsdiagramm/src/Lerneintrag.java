
public class Lerneintrag {
	

private String datum; 
private String fach; 
private String beschreibung; 
private int dauer;


public Lerneintrag(String datum, String fach, String beschreibung, int dauer) {
	this.datum = datum;
	this.fach = fach;
	this.beschreibung = beschreibung;
	this.dauer = dauer;
}


public String getDatum() {
	return datum;
}
public void setDatum(String datum) {
	this.datum = datum;
}
public String getFach() {
	return fach;
}
public void setFach(String fach) {
	this.fach = fach;
}
public String getBeschreibung() {
	return beschreibung;
}
public void setBeschreibung(String beschreibung) {
	this.beschreibung = beschreibung;
}
public int getDauer() {
	return dauer;
}
public void setDauer(int dauer) {
	this.dauer = dauer;
}


@Override
public String toString() {
	return "Lerneintrag [datum=" + datum + ", fach=" + fach + ", beschreibung=" + beschreibung + ", dauer=" + dauer
			+ "]";
}



}
