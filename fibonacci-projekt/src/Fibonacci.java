/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor: Maurice Lanwehr
   Version: 1.0
   Datum: 26.11
*/
public class Fibonacci {
	// Konstruktor
	Fibonacci() {
	}

	/**
	 * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboRekursiv(int n) {

	if(n<=0)            
		return 0;
	else if(n==1)
		return 1;
	else
	 return fiboRekursiv(n-2)+fiboRekursiv(n-1);
	} // fiboRekursiv

	/**
	 * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboIterativ(int n) {
	if(n<=0) 
		return 0;
	else if(n==1)
	    return 1;
	else
	{
	int a=0;  
	int b=1;  
	int i=2;
	while(i<=n) 
	{
	int aa=b; 
	int bb=a+b; 
	a=aa;  
	b=bb;  
	i++;
	}
    return b;
    }
	}// fiboIterativ

}// Fibonnaci
