
public class KeyStore01 {

  private String[] keyList;
  private int currentPos;

  /**
   * Constructs a new KeyStore for 100 keys.
   * 
   */
  public KeyStore01() {
    keyList = new String[100];
    currentPos=0;
  }
  
  /**
   * Constructs a new KeyStore 
   * 
   * @param  length - maximum number of elements that can be stored.
   */
  public KeyStore01(int length) {
    keyList = new String[length];
    currentPos=0;
  }

  /**
   *  Returns the element at the specified position in this list. 
   * 
   * @param index  -  index of the element to return
   * @return the element at the specified position in this list
   */
  public String get(int index) {
    return keyList[index];
  }

  /**
   *  Returns the number of elements in this list. 
   *  If this list contains more than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
   *  
   *  @return the number of elements in this list
   */
  public int size() {
    return currentPos;
  }

  /** 
   *  Removes all of the elements from this list (optional operation). 
   *  The list will be empty after this call returns.
   *  
   */
  public void clear() {
    currentPos = 0;
  }

  /**
   * Appends the specified element to the end of this list.
   * 
   * @param e - element to be appended to this list
   * @return true in successful case otherwise false
   */
  public boolean add(String e) {
    if (currentPos < keyList.length) {
      keyList[currentPos] = e;
      currentPos++;
      return true;
    } else
      return false;
  }

  /**
   * Removes the first occurrence of element at the specified position (index) in this list. 
   * If the array does not contain the element, it will stay unchanged.
   * 
   * @param index - index of the element to remove
   *            
   * @return true if this list contained the specified element <br>
   *         otherwise false
   */
  public boolean remove(int index) {
    if ((index >= 0) && (index < currentPos)) {
      for (int i = index; i < keyList.length - 1; i++) {
        keyList[i] = keyList[i + 1];
      } // for
      currentPos--;
      return true;
    } else
      return false;
  }
  
  /**
   * Removes the first occurrence of the specified element from array, 
   * if it is present. If the array does not contain the element, it will stay unchanged.
   * 
   * @param str  - string to be removed from this list, if present
   * @return true if this list contained the specified element <br>
   *         otherwise false
   */
  public boolean remove(String str) {
    int index = indexOf(str);
    if (index == -1)
      return false;
    return remove(index);
  }

  /**
   * Returns the index of the first occurrence of the specified element in this list, 
   * or -1 if this list does not contain the element.
   * 
   * @param str  -  element to search for
   *            
   * @return the index of the first occurrence of the specified element in this list, <br>
   *         or -1 if this list does not contain the element.
   */
  public int indexOf(String str) {
    int i = 0;
    while ((i < currentPos) && (!keyList[i].equals(str))) {
      i++;
    }
    if (i >= currentPos) 
      return (-1); 
    else
      return (i);
  }
  
  
  /**
   * Returns a string representation of the object. In general, the toString method returns 
   * a string that "textually represents" this object. The result should be a concise but 
   * informative representation that is easy for a person to read. 
   * For example "[ key1  key2  ... key1029 ]"
   *            
   * @return a string representation of the object.
   */
  public String toString(){
    String erg="[ ";
    for(int i=0 ; i < this.currentPos; i++)
      erg = erg + keyList[i] + " ";
    erg += "]";
    return erg;
  }

}
