import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileControl {
	
	public String dateiEinlesenUndAusgeben(File file) throws IOException {
	
	int counter = 0;
	String datum = null, fach = null, aktivitaet = null, dauer = null;
	StringBuffer text = new StringBuffer();
	
	try {
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

		
		String s = br.readLine();

		System.out.printf("%-10s %-11s %-44s %-8s ", "Datum", "| Fach", "| Aktivität", "| Dauer");
	    System.out.println("\n-----------------------------------------------------------------------------");
		
		text.append(String.format("\n%-12s %-15s %-45s %-23s", " Datum", "| Fach", "| Aktivität", "| Dauer"));
		text.append("\n -----------------------------------------------------------------------------------\n");

		while (s != null) {
			if (counter == 4 && s != null) {
		    System.out.printf("%-10s %-11s %-44s %-8s ", datum, "| " + fach, "| " + aktivitaet, "|  " + dauer);
			System.out.println();
				
				text.append(String.format("%-12s %-15s %-45s %-12s", " " + datum,"| " + fach, "| "+ aktivitaet, "|  "+ dauer));
				text.append("\n"); 
				
				counter = 0; 
			}
			
			if (counter <= 4) {
				System.out.println(s);
				s = br.readLine();

				if (counter == 0)
					datum = s;
				else if (counter == 1)
					fach = s;
				else if (counter == 2)
					aktivitaet = s;
				else if (counter == 3)
					dauer = s;
				
				counter++;
			}
		}
		
		br.close(); // von Musterlösung
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return text.toString();
}

}
