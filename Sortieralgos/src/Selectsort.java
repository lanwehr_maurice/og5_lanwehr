
public class Selectsort {
  
  public static void main(String[] args) {
    
    int[] unsortiert = {0,9,4,6,-2,-5,2,8,5,1,7,3,17,12,77,21,13,35,22};
    
    long time = System.currentTimeMillis();
    int[] sortiert = selectionsort(unsortiert);
    System.out.println(System.currentTimeMillis() - time + "ms");
    
    for (int i = 0; i < sortiert.length; i++) {
      System.out.print(sortiert[i] + ", ");
    }
  }
  
  public static int[] selectionsort(int[] sortieren) {
    for (int i = 0; i < sortieren.length - 1; i++) {
      for (int j = i + 1; j < sortieren.length; j++) {
        if (sortieren[i] > sortieren[j]) {
          int temp = sortieren[i];
          sortieren[i] = sortieren[j];
          sortieren[j] = temp;
          
          
        }
      }
    }
    
    return sortieren;
  } // end of main
  
  
} // end of class Selectsort