
public class Stoppuhr {

	
private long startpunkt = 0;
private long endpunkt = 0;


public Stoppuhr() {
	super();
}

public void starten() {
startpunkt = System.currentTimeMillis();
}

public void stoppen() {
endpunkt = System.currentTimeMillis();
}

public void reset() {
startpunkt = 0;
endpunkt = 0;
}

public long getDauer() {
return endpunkt - startpunkt;
}

}

