
public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){
	if (n == 0)
	return 1;
	else 
	return berechneFakultaet (n-1)* n;
	
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n 
	 */
	public static int berechneZweiHoch(int n){
	if (n > 31 || n == 0)
		return 1;	
	else 
		return berechneZweiHoch (n-1)* 2;		
	}
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n){
	if (n == 0 )
		return 0; 
	else 
	return berechneSumme (n-1) + n;
	
}
	
public static int qsum (int n) {
	if (n > 1)
		return n * n + qsum (n - 1);
	else
		return 1;
}

public static int add (int n, int y) {
	if (y == 0)
		return (n);
	else
		return (n + 1 + add(n,y-1));
}



}
	

