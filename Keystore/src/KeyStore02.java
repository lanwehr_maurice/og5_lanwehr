import java.util.*;

public class KeyStore02 {

	private ArrayList<String> keyList;
	private int length;

	public KeyStore02() {
		keyList = new ArrayList<String>();
		length = 0;
	}

	public int size() {
		return length;
	}

	public void clear() {
		length = 0;
	}
	
	public boolean add (String e){
	return keyList.add(e);
	}
	
	public boolean remove (int index){
		if (keyList.remove(index) != null)
			return true;
		return false;
	}
	
	public boolean remove (String str){
		return keyList.remove(str);
	}
	
	public int indexOf (String str){
		return keyList.indexOf(str);
	}
	
	public String toString(){
		return keyList.toString();
	}
}
