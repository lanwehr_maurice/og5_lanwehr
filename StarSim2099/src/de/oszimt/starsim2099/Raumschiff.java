package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (Oguzhan �zcan)
 * @version (v01 26.09.2016)
 */
public class Raumschiff {

	// Attribute
	private int posX;
	private int posY;
	private int maxLadekapazitaet;
	private int winkel;
	private String typ;
	private String antrieb;
	// Methoden
	
	public Raumschiff(){
		this.posX=0;
		this.posY=0;
		this.maxLadekapazitaet=0;
		this.winkel=0;
		this.typ="Unbekannt";
		this.antrieb="Unbekannt";
	}
	
	public Raumschiff(int posX,int posY, int maxLadekapazitaet, int winkel, String typ, String antrieb ){
		this.posX=posX;
		this.posY=posY;
		this.maxLadekapazitaet=maxLadekapazitaet;
		this.winkel=winkel;
		this.typ=typ;
		this.antrieb=antrieb;
	}
	
	
	
	
	
	
	
	
	
	
	public int getPosX() {
		return posX;
	}




	public void setPosX(int posX) {
		this.posX = posX;
	}




	public int getPosY() {
		return posY;
	}




	public void setPosY(int posY) {
		this.posY = posY;
	}



	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}



	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}



	public int getWinkel() {
		return winkel;
	}



	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}



	public String getTyp() {
		return typ;
	}



	public void setTyp(String typ) {
		this.typ = typ;
	}



	public String getAntrieb() {
		return antrieb;
	}



	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	
	
	
	
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}




	

}
