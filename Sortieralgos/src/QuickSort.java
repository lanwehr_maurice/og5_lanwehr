
import java.util.Random;

public class QuickSort {
  
  public static void sortiere (int x[]) {
   quickSort(x, 0, x.length - 1);
  }
  
  public static void quickSort(int x[], int links, int rechts) {
    if (links < rechts) {
      int i = splitten(x, links, rechts);
      quickSort(x, links, i - 1);
      quickSort(x, i + 1, rechts);
    }
  }
  
  public static int splitten (int x[], int links, int rechts) {
    
    int angelpunkt, i, j, help;
    angelpunkt = x[rechts];               
    i  = links;
    j  = rechts - 1;
    
    while( i<=j ) {
      if (x[i] > angelpunkt) {     
        // tausche x[i] und x[j]
        help = x[i]; 
        x[i] = x[j]; 
        x[j] = help;                             
        j--;
      } else i++;            
    }
    
    // tausche x[i] und x[rechts]
    help  = x[i];
    x[i]  = x[rechts];
    x[rechts] = help;
    
    return i;
  }
  
  public static void main(String[] args) {
    
    
    
    int[] array = {0,9,4,6,-2,-5,2,8,5,1,7,3,17,12,77,21,13,35,22};
    long time = System.currentTimeMillis();
    sortiere(array);
    System.out.println(System.currentTimeMillis() - time + "ms");
    for (int i = 0; i < array.length; i++) 
    System.out.print(array[i] + " ");         
  }    
}