package git_taschenrechner;

import java.util.Scanner;

public class Taschenrechnertest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();
		
	
	    System.out.println("Bitte geben sie die erste Zahl ein:");
	    int zahl1 = myScanner.nextInt();
	    
	    System.out.println("Bitte geben sie die zweite Zahl ein:");
	    int zahl2 = myScanner.nextInt();
 

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Dividieren     |");
		System.out.println("|        4. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println("zahl1 + zahl2 = "  + ts.add(zahl1, zahl2));
			break;
			
		case '2':
			System.out.println("zahl1 - zahl2 = " + ts.sub(zahl1, zahl2));
			break;
			
		case '3':
			System.out.println("zahl1 / zahl2 = " + ts.div(zahl1, zahl2));
			break;
			
		case '4':
			System.out.println("zahl1 * zahl2 = " + ts.mul(zahl1, zahl2));
			break;
			
		  //  add your code here
		  
		  
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
			
		
		}

	}

}