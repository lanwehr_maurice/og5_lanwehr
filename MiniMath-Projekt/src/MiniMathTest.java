/** MiniMathTest.java
 *
 * Testprogramm f�r verschiedene Berechnungen.
 *
 */
import java.util.Scanner;

class MiniMathTest {

	public static void main(String arg[]) {
	
	Scanner myScanner = new Scanner(System.in);
    char operator;
    int n;
    int y;
    System.out.println("Auswahl");
    System.out.println(" (Z)weierpotenz");
    System.out.println(" (F)akultaet ");
    System.out.println(" (S)umme ");
    System.out.println(" (E)xit ");
    System.out.println(" (Q)sum ");
    System.out.println(" (A)dd ");
    do {
      System.out.print("Ihre Auswahl:\t");
      operator = myScanner.next().charAt(0);
      switch(operator) {
        case 'z': ;
        case 'Z':
             System.out.print("     Exponent:  ");
             n = myScanner.nextInt();
             System.out.println("\t\t2 ^ "+n+" = "+ MiniMath.berechneZweiHoch(n)+"\n");
             break;
        case 'f': ;
        case 'F':
             System.out.print("     Argument:  ");
             n = myScanner.nextInt();
             System.out.println("\t\t"+n+"! = "+ MiniMath.berechneFakultaet(n)+"\n");
             break;
        case 's': ;
        case 'S':
             System.out.print("     Argument:  ");
             n = myScanner.nextInt();
             System.out.println("\t\tSumme von 1 bis "+n+" = "+ MiniMath.berechneSumme(n)+"\n");
             break;
        case 'q': ;
        case 'Q':
             System.out.print("     Argument:  ");
             n = myScanner.nextInt();
             System.out.println("qsum: "+n+" = "+ MiniMath.qsum(n)+"\n");
             break;
        case 'a': ;
        case 'A':
             System.out.print("     1. Argument:  ");
             n = myScanner.nextInt();
             System.out.print("     2. Argument:  ");
             y = myScanner.nextInt();
             System.out.println("add: "+ MiniMath.add(n,y)+"\n");
             break;
        case 'e': ;
        case 'E':
            System.exit(0);
      } //switch
    } while(true);
  }//main
} // MiniMathTest

