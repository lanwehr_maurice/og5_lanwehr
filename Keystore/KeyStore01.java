
public class KeyStore01 {
	private String[] keylist;
	private int currentPos;

	public KeyStore01() {
		keylist = new String[100];
		currentPos = 0;
	}

	public KeyStore01(int length) {
		keylist = new String[length];
	}

	public String get(int index) {
		if (index <= keylist.length - 1)
			return keylist[index];
		else
			return null;

	}

	public int size() {
		return currentPos;
	}

	public void clear() {
		keylist = 0;
	}

	public boolean add(String e) {
		if (currentPos >= keylist.length)
			return false;
		else
			keylist[currentPos] = e;
		currentPos++;
		return true;
	}

	public boolean remove(int index) {

	}

	public boolean remove(String str) {

	}

	public int indexOf(String str) {

	}

	public String toString() {

	}

}
