
public class Addon {

	// Anfang Attribute
	private int idnummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int maxbestand;
	private int aktuellerbestand;
	// Ende Attribute

	public Addon() {
		this.idnummer = 0;
		this.aktuellerbestand = 0;
		this.bezeichnung = "";
		this.verkaufspreis = 0;
		this.maxbestand = 0;
	} 

	// Anfang Methoden
	public Addon(int idnummer, String bezeichnung, double verkaufspreis, int maxbestand, int aktuellerbestand) {
		this.idnummer = idnummer;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.maxbestand = maxbestand;
		this.aktuellerbestand = aktuellerbestand;
	}

	public int aendereBestand() {
		return 0;
	}

	public int getIdnummer() {
		return idnummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public int getMaxbestand() {
		return maxbestand;
	}

	public void setIdnummer(int idnummer) {
		this.idnummer = idnummer;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public void setMaxbestand(int maxbestand) {
		this.maxbestand = maxbestand;
	}

	public int getAktuellerbestand() {
		return aktuellerbestand;
	}

	public void setAktuellerbestand(int aktuellerbestand) {
		this.aktuellerbestand = aktuellerbestand;
	}

	public int berechneGesamtwert() {
		return 0;
	}

	// Ende Methoden
}
